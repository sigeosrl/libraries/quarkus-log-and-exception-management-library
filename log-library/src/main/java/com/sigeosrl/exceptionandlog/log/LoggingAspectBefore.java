package com.sigeosrl.exceptionandlog.log;


import io.quarkus.arc.profile.IfBuildProfile;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.Dependent;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;


@Dependent
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
@LoggingBefore
public class LoggingAspectBefore {

    @Getter
    @Setter
    private Instant start;

    private final SecurityIdentity securityIdentity;

    public LoggingAspectBefore(SecurityIdentity securityIdentity) {
        this.securityIdentity = securityIdentity;
    }

    @AroundInvoke
    @LoggingBefore
    public Object logBefore(InvocationContext ctx) throws Exception {
        String methodName = ctx.getMethod().getName();
        String clazz = ctx.getTarget().getClass().getSimpleName();
        Log.logStart(clazz, methodName, securityIdentity.getPrincipal().getName());
        start = Instant.now();
        return ctx.proceed();
    }
}