package com.sigeosrl.exceptionandlog.log;


import com.sigeosrl.exceptionandlog.exception.*;
import io.quarkus.arc.profile.IfBuildProfile;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.Dependent;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.time.Instant;


@Dependent
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
@LoggingAfterThrowing
@Slf4j
public class LoggingAspectAfterThrowing {

    private final SecurityIdentity securityIdentity;
    private final LoggingAspectAfter loggingAspectAfter;
    private final LoggingAspectBefore loggingAspectBefore;

    public LoggingAspectAfterThrowing(SecurityIdentity securityIdentity) {
        this.securityIdentity = securityIdentity;
        this.loggingAspectBefore = new LoggingAspectBefore(securityIdentity);
        this.loggingAspectAfter = new LoggingAspectAfter(securityIdentity);
    }


    @AroundInvoke
    @LoggingAfterThrowing
    public Object logAfterThrowing(InvocationContext ctx) throws Exception {

        loggingAspectBefore.setStart(Instant.now());
        Instant start = loggingAspectBefore.getStart();

        loggingAspectAfter.setIsAfterThrowing(true);

        Object result;
        try {
            // Proceed with the method invocation
            result = ctx.proceed();
            // Log method completion if not throwing an exception
            Method method = ctx.getMethod();
            LoggingAfterThrowing loggingAfterThrowing = method.getAnnotation(LoggingAfterThrowing.class);
            if (loggingAfterThrowing != null) {
                String methodName = ctx.getMethod().getName();
                String clazz = ctx.getTarget().getClass().getSimpleName();
                Log.logException(clazz, methodName, securityIdentity.getPrincipal().getName(), "500 INTERNAL SERVER ERROR", start, (Throwable) ctx.getParameters()[0]);
            }
        } catch (Exception e) {
            // Log if an exception is thrown
            Log.logException(ctx.getTarget().getClass().getSimpleName(), ctx.getMethod().getName(), securityIdentity.getPrincipal().getName(), checkStatus(e), start, e);
            throw e;
        }
        return result;
    }

    private String checkStatus(Throwable e) {
        return switch (e) {
            case BadRequestException ignored -> "400 BAD REQUEST";
            case ConflictException ignored -> "409 CONFLICT";
            case ExpectationFailedException ignored -> "417 EXPECTATION FAILED";
            case FailedDependencyException ignored -> "424 FAILED DEPENDENCY";
            case ForbiddenException ignored -> "403 FORBIDDEN";
            case GoneException ignored -> "410 GONE";
            case LengthRequiredException ignored -> "411 LENGTH REQUIRED";
            case LockedException ignored -> "423 LOCKED";
            case NotAcceptableException ignored -> "406 NOT ACCEPTABLE";
            case NotFoundException ignored -> "404 NOT FOUND";
            case PaymentRequiredException ignored -> "402 PAYMENT REQUIRED";
            case PreconditionFailedException ignored -> "412 PRECONDITION FAILED";
            case PreconditionRequiredException ignored -> "428 PRECONDITION REQUIRED";
            case TeapotException ignored -> "418 IM A TEAPOT";
            case UnprocessableEntityException ignored -> "422 UNPROCESSABLE ENTITY";
            case UnsupportedMediaTypeException ignored -> "415 UNSUPPORTED MEDIA TYPE";
            default -> "500 INTERNAL SERVER ERROR";
        };
    }
}