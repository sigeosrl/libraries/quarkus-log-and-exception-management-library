package com.sigeosrl.exceptionandlog.log;

import jakarta.interceptor.InterceptorBinding;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@InterceptorBinding
public @interface LoggingAfterThrowing {
}