package com.sigeosrl.exceptionandlog.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTraceUtil {

    private StackTraceUtil() {}

    public static String getPrettyStackTrace(Throwable throwable) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        throwable.printStackTrace(printWriter);
        return stringWriter.toString();
    }
}