package com.sigeosrl.exceptionandlog.security;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

@CheckMatchingAuthority(pattern = "")
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class MatchingAuthorityInterceptor {

    @Inject
    SecurityIdentity securityIdentity;

    @AroundInvoke
    public Object checkSecurity(InvocationContext context) throws Exception {
        CheckMatchingAuthority annotation = context.getMethod().getAnnotation(CheckMatchingAuthority.class);
        if (annotation == null) {
            annotation = context.getTarget().getClass().getAnnotation(CheckMatchingAuthority.class);
        }

        if (securityIdentity.isAnonymous()) {
            throw new SecurityException("Anonymous access not allowed");
        }

        CheckMatchingAuthority finalAnnotation = annotation;
        boolean hasAuthority = securityIdentity.getRoles().stream()
                .anyMatch(role -> role.matches(finalAnnotation.pattern()));

        if (!hasAuthority) {
            throw new SecurityException("User does not have the required authority");
        }

        return context.proceed();
    }
}