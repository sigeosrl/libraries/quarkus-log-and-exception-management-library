package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.PreconditionRequiredException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.PRECONDITION_REQUIRED;

@IfBuildProfile("dev")
@Provider
public class DevPreconditionRequiredExceptionHandler implements ExceptionMapper<PreconditionRequiredException> {

    @Override
    public Response toResponse(PreconditionRequiredException exception) {
        return Response.status(PRECONDITION_REQUIRED)
                .entity(new DevErrorResponse(PRECONDITION_REQUIRED, exception.getMessage(), exception.getStackTrace(), PRECONDITION_REQUIRED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
