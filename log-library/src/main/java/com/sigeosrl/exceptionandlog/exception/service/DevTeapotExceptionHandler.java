package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.TeapotException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@IfBuildProfile("dev")
@Provider
public class DevTeapotExceptionHandler implements ExceptionMapper<TeapotException> {

    @Override
    public Response toResponse(TeapotException exception) {
        return Response.status(418)
                .entity(new DevErrorResponse(null, exception.getMessage(), exception.getStackTrace(), 418))
                .type(APPLICATION_JSON)
                .build();
    }
}
