package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.NotFoundException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;

@IfBuildProfile("dev")
@Provider
public class DevNotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        return Response.status(NOT_FOUND)
                .entity(new DevErrorResponse(NOT_FOUND, exception.getMessage(), exception.getStackTrace(), NOT_FOUND.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
