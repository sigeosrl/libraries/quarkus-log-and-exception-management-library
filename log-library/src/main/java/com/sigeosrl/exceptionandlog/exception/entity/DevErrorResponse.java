package com.sigeosrl.exceptionandlog.exception.entity;

import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@IfBuildProfile("dev")
public class DevErrorResponse {
    private Response.Status status;
    private String message;
    private StackTraceElement[] stackTrace;
    private Integer originalStatus;
}
