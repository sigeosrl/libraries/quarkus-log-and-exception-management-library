package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.UnsupportedMediaTypeException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.UNSUPPORTED_MEDIA_TYPE;

@UnlessBuildProfile("dev")
@Provider
public class DefaultUnsupportedMediaTypeExceptionHandler implements ExceptionMapper<UnsupportedMediaTypeException> {

    @Override
    public Response toResponse(UnsupportedMediaTypeException exception) {
        return Response.status(UNSUPPORTED_MEDIA_TYPE)
                .entity(new DefaultErrorResponse(UNSUPPORTED_MEDIA_TYPE, exception.getMessage(), UNSUPPORTED_MEDIA_TYPE.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
