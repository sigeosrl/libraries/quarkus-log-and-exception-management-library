package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.LengthRequiredException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.LENGTH_REQUIRED;

@IfBuildProfile("dev")
@Provider
public class DevLengthRequiredExceptionHandler implements ExceptionMapper<LengthRequiredException> {

    @Override
    public Response toResponse(LengthRequiredException exception) {
        return Response.status(LENGTH_REQUIRED)
                .entity(new DevErrorResponse(LENGTH_REQUIRED, exception.getMessage(), exception.getStackTrace(), LENGTH_REQUIRED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
