package com.sigeosrl.exceptionandlog.exception.service;


import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.NotFoundException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.*;

@UnlessBuildProfile("dev")
@Provider
public class DefaultNotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        return Response.status(NOT_FOUND)
                .entity(new DefaultErrorResponse(NOT_FOUND, exception.getMessage(), NOT_FOUND.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
