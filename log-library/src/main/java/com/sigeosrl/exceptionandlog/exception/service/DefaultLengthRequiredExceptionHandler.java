package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.LengthRequiredException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.LENGTH_REQUIRED;

@UnlessBuildProfile("dev")
@Provider
public class DefaultLengthRequiredExceptionHandler implements ExceptionMapper<LengthRequiredException> {

    @Override
    public Response toResponse(LengthRequiredException exception) {
        return Response.status(LENGTH_REQUIRED)
                .entity(new DefaultErrorResponse(LENGTH_REQUIRED, exception.getMessage(), LENGTH_REQUIRED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
