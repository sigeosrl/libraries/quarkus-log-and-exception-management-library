package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.PreconditionRequiredException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.PRECONDITION_REQUIRED;

@UnlessBuildProfile("dev")
@Provider
public class DefaultPreconditionRequiredExceptionHandler implements ExceptionMapper<PreconditionRequiredException> {

    @Override
    public Response toResponse(PreconditionRequiredException exception) {
        return Response.status(PRECONDITION_REQUIRED)
                .entity(new DefaultErrorResponse(PRECONDITION_REQUIRED, exception.getMessage(), PRECONDITION_REQUIRED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
