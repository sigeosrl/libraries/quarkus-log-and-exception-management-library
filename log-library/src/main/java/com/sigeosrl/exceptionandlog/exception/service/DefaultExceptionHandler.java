package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@UnlessBuildProfile("dev")
@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable ex) {
        DefaultErrorResponse defaultErrorResponse = new DefaultErrorResponse(INTERNAL_SERVER_ERROR, ex.getMessage(), INTERNAL_SERVER_ERROR.getStatusCode());
        return Response.status(INTERNAL_SERVER_ERROR)
                .entity(defaultErrorResponse)
                .type(APPLICATION_JSON)
                .build();
    }
}