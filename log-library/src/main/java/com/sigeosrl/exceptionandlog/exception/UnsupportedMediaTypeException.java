package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class UnsupportedMediaTypeException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public UnsupportedMediaTypeException() {
        super();
    }

    public UnsupportedMediaTypeException(String message) {
        super(message);
    }

    public UnsupportedMediaTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
