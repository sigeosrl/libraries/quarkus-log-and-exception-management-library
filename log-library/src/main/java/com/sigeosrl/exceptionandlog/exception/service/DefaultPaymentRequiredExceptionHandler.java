package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.PaymentRequiredException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.PAYMENT_REQUIRED;

@UnlessBuildProfile("dev")
@Provider
public class DefaultPaymentRequiredExceptionHandler implements ExceptionMapper<PaymentRequiredException> {

    @Override
    public Response toResponse(PaymentRequiredException exception) {
        return Response.status(PAYMENT_REQUIRED)
                .entity(new DefaultErrorResponse(PAYMENT_REQUIRED, exception.getMessage(), PAYMENT_REQUIRED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
