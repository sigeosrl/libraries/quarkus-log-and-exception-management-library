package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.UnprocessableEntityException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@IfBuildProfile("dev")
@Provider
public class DevUnprocessableEntityExceptionHandler implements ExceptionMapper<UnprocessableEntityException> {

    @Override
    public Response toResponse(UnprocessableEntityException exception) {
        return Response.status(422)
                .entity(new DevErrorResponse(null, exception.getMessage(), exception.getStackTrace(), 422))
                .type(APPLICATION_JSON)
                .build();
    }
}
