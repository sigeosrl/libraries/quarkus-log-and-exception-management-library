package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class LockedException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public LockedException() {
        super();
    }

    public LockedException(String msg) {
        super(msg);
    }

    public LockedException(String message, Throwable cause) {
        super(message, cause);
    }
}
