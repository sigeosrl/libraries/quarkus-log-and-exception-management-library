package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.ExpectationFailedException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.EXPECTATION_FAILED;

@UnlessBuildProfile("dev")
@Provider
public class DefaultExpecationFailedExceptionHandler implements ExceptionMapper<ExpectationFailedException> {

    @Override
    public Response toResponse(ExpectationFailedException exception) {
        return Response.status(EXPECTATION_FAILED)
                .entity(new DefaultErrorResponse(EXPECTATION_FAILED, exception.getMessage(), EXPECTATION_FAILED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
