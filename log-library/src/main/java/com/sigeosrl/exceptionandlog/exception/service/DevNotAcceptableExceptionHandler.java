package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.NotAcceptableException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.NOT_ACCEPTABLE;

@IfBuildProfile("dev")
@Provider
public class DevNotAcceptableExceptionHandler implements ExceptionMapper<NotAcceptableException> {

    @Override
    public Response toResponse(NotAcceptableException exception) {
        return Response.status(NOT_ACCEPTABLE)
                .entity(new DevErrorResponse(NOT_ACCEPTABLE, exception.getMessage(), exception.getStackTrace(), NOT_ACCEPTABLE.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
