package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class TeapotException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public TeapotException() {
        super();
    }

    public TeapotException(String message) {
        super(message);
    }

    public TeapotException(String message, Throwable cause) {
        super(message, cause);
    }
}
