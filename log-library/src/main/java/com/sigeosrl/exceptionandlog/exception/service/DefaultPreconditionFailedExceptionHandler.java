package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.PreconditionFailedException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.PRECONDITION_FAILED;

@UnlessBuildProfile("dev")
@Provider
public class DefaultPreconditionFailedExceptionHandler implements ExceptionMapper<PreconditionFailedException> {

    @Override
    public Response toResponse(PreconditionFailedException exception) {
        return Response.status(PRECONDITION_FAILED)
                .entity(new DefaultErrorResponse(PRECONDITION_FAILED, exception.getMessage(), PRECONDITION_FAILED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
