package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class BadRequestException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public BadRequestException() {
        super();
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
