package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.FailedDependencyException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@IfBuildProfile("dev")
@Provider
public class DevFailedDependencyExceptionHandler implements ExceptionMapper<FailedDependencyException> {

    @Override
    public Response toResponse(FailedDependencyException exception) {
        return Response.status(424)
                .entity(new DevErrorResponse(null, exception.getMessage(), exception.getStackTrace(), 424))
                .type(APPLICATION_JSON)
                .build();
    }
}
