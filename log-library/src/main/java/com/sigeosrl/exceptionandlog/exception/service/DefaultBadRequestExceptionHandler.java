package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.BadRequestException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.BAD_REQUEST;

@UnlessBuildProfile("dev")
@Provider
public class DefaultBadRequestExceptionHandler implements ExceptionMapper<BadRequestException> {

    @Override
    public Response toResponse(BadRequestException exception) {
        return Response.status(BAD_REQUEST)
                .entity(new DefaultErrorResponse(BAD_REQUEST, exception.getMessage(), BAD_REQUEST.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
