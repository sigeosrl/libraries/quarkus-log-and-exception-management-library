package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class LengthRequiredException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public LengthRequiredException() {
        super();
    }

    public LengthRequiredException(String message) {
        super(message);
    }

    public LengthRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
