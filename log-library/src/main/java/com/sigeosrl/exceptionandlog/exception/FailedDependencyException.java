package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class FailedDependencyException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public FailedDependencyException() {
        super();
    }

    public FailedDependencyException(String message) {
        super(message);
    }

    public FailedDependencyException(String message, Throwable cause) {
        super(message, cause);
    }
}
