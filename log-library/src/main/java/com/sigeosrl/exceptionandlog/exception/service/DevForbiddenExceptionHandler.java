package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.ForbiddenException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.FORBIDDEN;

@IfBuildProfile("dev")
@Provider
public class DevForbiddenExceptionHandler implements ExceptionMapper<ForbiddenException> {

    @Override
    public Response toResponse(ForbiddenException exception) {
        return Response.status(FORBIDDEN)
                .entity(new DevErrorResponse(FORBIDDEN, exception.getMessage(), exception.getStackTrace(), FORBIDDEN.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
