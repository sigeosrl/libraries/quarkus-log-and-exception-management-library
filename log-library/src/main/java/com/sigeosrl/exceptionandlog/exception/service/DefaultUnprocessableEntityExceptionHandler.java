package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.UnprocessableEntityException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@UnlessBuildProfile("dev")
@Provider
public class DefaultUnprocessableEntityExceptionHandler implements ExceptionMapper<UnprocessableEntityException> {

    @Override
    public Response toResponse(UnprocessableEntityException exception) {
        return Response.status(422)
                .entity(new DefaultErrorResponse(null, exception.getMessage(), 422))
                .type(APPLICATION_JSON)
                .build();
    }
}
