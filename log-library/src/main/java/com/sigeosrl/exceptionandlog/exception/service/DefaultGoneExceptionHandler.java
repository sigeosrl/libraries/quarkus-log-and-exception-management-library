package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.GoneException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.GONE;

@UnlessBuildProfile("dev")
@Provider
public class DefaultGoneExceptionHandler implements ExceptionMapper<GoneException> {

    @Override
    public Response toResponse(GoneException exception) {
        return Response.status(GONE)
                .entity(new DefaultErrorResponse(GONE, exception.getMessage(), GONE.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
