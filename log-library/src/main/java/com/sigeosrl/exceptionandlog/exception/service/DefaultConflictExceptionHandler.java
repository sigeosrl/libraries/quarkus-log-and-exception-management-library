package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.ConflictException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.CONFLICT;

@UnlessBuildProfile("dev")
@Provider
public class DefaultConflictExceptionHandler implements ExceptionMapper<ConflictException> {

    @Override
    public Response toResponse(ConflictException exception) {
        return Response.status(CONFLICT)
                .entity(new DefaultErrorResponse(CONFLICT, exception.getMessage(), CONFLICT.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
