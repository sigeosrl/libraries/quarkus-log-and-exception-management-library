package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.GoneException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.GONE;

@IfBuildProfile("dev")
@Provider
public class DevGoneExceptionHandler implements ExceptionMapper<GoneException> {

    @Override
    public Response toResponse(GoneException exception) {
        return Response.status(GONE)
                .entity(new DevErrorResponse(GONE, exception.getMessage(), exception.getStackTrace(), GONE.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
