package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.LockedException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@IfBuildProfile("dev")
@Provider
public class DevLockedExceptionHandler implements ExceptionMapper<LockedException> {

    @Override
    public Response toResponse(LockedException exception) {
        return Response.status(423)
                .entity(new DevErrorResponse(null, exception.getMessage(), exception.getStackTrace(), 423))
                .type(APPLICATION_JSON)
                .build();
    }
}
