package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.PreconditionFailedException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.PRECONDITION_FAILED;

@IfBuildProfile("dev")
@Provider
public class DevPreconditionFailedExceptionHandler implements ExceptionMapper<PreconditionFailedException> {

    @Override
    public Response toResponse(PreconditionFailedException exception) {
        return Response.status(PRECONDITION_FAILED)
                .entity(new DevErrorResponse(PRECONDITION_FAILED, exception.getMessage(), exception.getStackTrace(), PRECONDITION_FAILED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
