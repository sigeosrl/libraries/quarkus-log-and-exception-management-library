package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.NotAcceptableException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.NOT_ACCEPTABLE;

@UnlessBuildProfile("dev")
@Provider
public class DefaultNotAcceptableExceptionHandler implements ExceptionMapper<NotAcceptableException> {

    @Override
    public Response toResponse(NotAcceptableException exception) {
        return Response.status(NOT_ACCEPTABLE)
                .entity(new DefaultErrorResponse(NOT_ACCEPTABLE, exception.getMessage(), NOT_ACCEPTABLE.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
