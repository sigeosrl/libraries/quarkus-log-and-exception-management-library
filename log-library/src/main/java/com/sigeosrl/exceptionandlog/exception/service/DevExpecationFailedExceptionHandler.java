package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.ExpectationFailedException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.EXPECTATION_FAILED;

@IfBuildProfile("dev")
@Provider
public class DevExpecationFailedExceptionHandler implements ExceptionMapper<ExpectationFailedException> {

    @Override
    public Response toResponse(ExpectationFailedException exception) {
        return Response.status(EXPECTATION_FAILED)
                .entity(new DevErrorResponse(EXPECTATION_FAILED, exception.getMessage(), exception.getStackTrace(), EXPECTATION_FAILED.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
