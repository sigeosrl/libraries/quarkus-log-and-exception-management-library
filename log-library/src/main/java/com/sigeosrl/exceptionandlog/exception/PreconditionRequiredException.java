package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class PreconditionRequiredException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public PreconditionRequiredException() {
        super();
    }

    public PreconditionRequiredException(String message) {
        super(message);
    }

    public PreconditionRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
