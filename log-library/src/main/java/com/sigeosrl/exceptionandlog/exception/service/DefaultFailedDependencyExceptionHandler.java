package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.FailedDependencyException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@UnlessBuildProfile("dev")
@Provider
public class DefaultFailedDependencyExceptionHandler implements ExceptionMapper<FailedDependencyException> {

    @Override
    public Response toResponse(FailedDependencyException exception) {
        return Response.status(424)
                .entity(new DefaultErrorResponse(null, exception.getMessage(), 424))
                .type(APPLICATION_JSON)
                .build();
    }
}
