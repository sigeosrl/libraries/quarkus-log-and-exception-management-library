package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class ExpectationFailedException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public ExpectationFailedException() {
        super();
    }

    public ExpectationFailedException(String message) {
        super(message);
    }

    public ExpectationFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
