package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.LockedException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@UnlessBuildProfile("dev")
@Provider
public class DefaultLockedExceptionHandler implements ExceptionMapper<LockedException> {

    @Override
    public Response toResponse(LockedException exception) {
        return Response.status(423)
                .entity(new DefaultErrorResponse(null, exception.getMessage(), 423))
                .type(APPLICATION_JSON)
                .build();
    }
}
