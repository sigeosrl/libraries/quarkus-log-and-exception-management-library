package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@IfBuildProfile("dev")
@Provider
@Slf4j
public class DevExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable ex) {
        return Response.status(INTERNAL_SERVER_ERROR)
                .entity(new DevErrorResponse(INTERNAL_SERVER_ERROR, ex.getMessage(), ex.getStackTrace(), INTERNAL_SERVER_ERROR.getStatusCode()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}