package com.sigeosrl.exceptionandlog.exception.entity;

import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@UnlessBuildProfile("dev")
public class DefaultErrorResponse {
    private Response.Status status;
    private String error;
    private Integer originalStatus;
}
