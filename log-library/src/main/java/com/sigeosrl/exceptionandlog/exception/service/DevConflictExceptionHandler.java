package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DevErrorResponse;
import com.sigeosrl.exceptionandlog.exception.ConflictException;
import io.quarkus.arc.profile.IfBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.Response.Status.CONFLICT;

@IfBuildProfile("dev")
@Provider
public class DevConflictExceptionHandler implements ExceptionMapper<ConflictException> {

    @Override
    public Response toResponse(ConflictException exception) {
        return Response.status(CONFLICT)
                .entity(new DevErrorResponse(CONFLICT, exception.getMessage(), exception.getStackTrace(), CONFLICT.getStatusCode()))
                .type(APPLICATION_JSON)
                .build();
    }
}
