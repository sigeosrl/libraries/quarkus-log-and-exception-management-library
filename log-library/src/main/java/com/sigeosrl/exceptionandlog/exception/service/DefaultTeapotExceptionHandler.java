package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.entity.DefaultErrorResponse;
import com.sigeosrl.exceptionandlog.exception.TeapotException;
import io.quarkus.arc.profile.UnlessBuildProfile;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@UnlessBuildProfile("dev")
@Provider
public class DefaultTeapotExceptionHandler implements ExceptionMapper<TeapotException> {

    @Override
    public Response toResponse(TeapotException exception) {
        return Response.status(418)
                .entity(new DefaultErrorResponse(null, exception.getMessage(), 418))
                .type(APPLICATION_JSON)
                .build();
    }
}
