package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class GoneException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public GoneException() {
        super();
    }

    public GoneException(String msg) {
        super(msg);
    }

    public GoneException(String message, Throwable cause) {
        super(message, cause);
    }
}
