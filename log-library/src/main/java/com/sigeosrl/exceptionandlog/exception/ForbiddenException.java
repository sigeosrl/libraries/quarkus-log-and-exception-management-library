package com.sigeosrl.exceptionandlog.exception;

import java.io.Serial;
import java.io.Serializable;

public class ForbiddenException extends RuntimeException implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public ForbiddenException() {
        super();
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }
}
