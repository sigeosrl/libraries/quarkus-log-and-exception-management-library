package com.sigeosrl.exceptionandlog.extension;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

public class ExtensionProcessor {

    private static final String FEATURE_NAME = "Logs and Exceptions Library";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE_NAME);
    }
}
