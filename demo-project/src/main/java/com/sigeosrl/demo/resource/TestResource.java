package com.sigeosrl.demo.resource;

import com.sigeosrl.exceptionandlog.exception.ConflictException;
import com.sigeosrl.exceptionandlog.log.LoggingAfter;
import com.sigeosrl.exceptionandlog.log.LoggingAfterThrowing;
import com.sigeosrl.exceptionandlog.log.LoggingBefore;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/demo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TestResource {

    @GET
    @LoggingBefore
    @LoggingAfterThrowing
    public Response testException() throws ConflictException {
        throw new ConflictException("Test exception");
    }

    @GET
    @Path("/hello")
    @LoggingBefore
    @LoggingAfter
    public Response hello() {
        return Response.ok("Hello World!").build();
    }
}
